Normal resumes are timelines — this resume is a timeline of timelines. 

See what I've been up to, and how my resume has evolved over time.